package by.itstep.channel.controller;

import by.itstep.channel.dto.user.UserCreateDto;
import by.itstep.channel.dto.user.UserFullDto;
import by.itstep.channel.entity.UserEntity;
import by.itstep.channel.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.regex.MatchResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class userControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void clearDatabase() {
        userRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        UserEntity userToSave = new UserEntity();
        userToSave.setEmail("@mail");
        userToSave.setLogin("Bob");
        userToSave.setPassword("44444");
        userToSave.setImageUrl("jpg");

        UserEntity savedUser = userRepository.save(userToSave);

        //when
        MvcResult result = mockMvc.perform(get("/users/{id}", savedUser.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);

        //then
        Assertions.assertNotNull(foundUser);
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(savedUser.getId(), foundUser.getId());
    }

    @Test
    void testFindById_whenNotFound() throws Exception {
        //given
        int notExisting = 1000;

        //when
        mockMvc.perform(get("/users/{id}", notExisting))
                .andExpect(status().isNotFound());
    }

    @Test
    void testCreate_happyPath() throws Exception {
        //given
        UserCreateDto userToSave = new UserCreateDto();
        userToSave.setEmail("jhg@mail");
        userToSave.setLogin("Bob");
        userToSave.setPassword("44444");
        userToSave.setImageUrl("jpg");

        //when
        MvcResult result = mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userToSave)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        UserFullDto savedUser = objectMapper.readValue(bytes, UserFullDto.class);

        //then
        Assertions.assertNotNull(savedUser);
    }

    @Test
    void testCreate_whenEmailIsEmpty() throws Exception {
        //given
        UserCreateDto userToSave = new UserCreateDto();
        userToSave.setEmail(null);
        userToSave.setLogin("Bob");
        userToSave.setPassword("44444");
        userToSave.setImageUrl("jpg");

        //when
        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userToSave)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testFindAll_happyPath() throws Exception {
        //given
        UserEntity userToSave = new UserEntity();
        userToSave.setEmail("bob@mail");
        userToSave.setLogin("Boby");
        userToSave.setPassword("44444");
        userToSave.setImageUrl("jpg");

        UserEntity userToSave2 = new UserEntity();
        userToSave2.setEmail("axe@mail");
        userToSave2.setLogin("Bob");
        userToSave2.setPassword("44477");
        userToSave2.setImageUrl("img");

        userRepository.save(userToSave);
        userRepository.save(userToSave2);

        //when
        MvcResult result = mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<UserFullDto> foundUsers =objectMapper.readValue(bytes,
                new TypeReference<List<UserFullDto>>() { });

        //then
        Assertions.assertNotNull(foundUsers);
        Assertions.assertEquals(2, foundUsers.size());
    }
}
