package by.itstep.channel.repository;

import by.itstep.channel.entity.ChannelEntity;
import by.itstep.channel.entity.PostEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.channel.repository.EntityGenerationUtils.generateChannel;
import static by.itstep.channel.repository.EntityGenerationUtils.generatePosts;

@SpringBootTest
public class ChannelRepositoryTest {

    @Autowired
    private ChannelRepository channelRepository;

//    @Test
//    void findById_happyPath() {
//        // given
//        ChannelEntity toSave = generateChannel();
//        List<PostEntity> posts = generatePosts();// получаем список рандомных постов
//
//        toSave.setPosts(posts); //связываем entity между собой
//        for (PostEntity p : posts) {
//            p.setChannel(toSave); // а теперь каждый транзитивный entity знает про свой канал
//        }
//
//        ChannelEntity saved = channelRepository.save(toSave);// в момент сохранения, toSave
//        // содержит 3 новых поста
//        Integer idToSearch = saved.getId();// берем id сохраненного элемента,
//        // чтобы потом его найти в БД
//
//        // when
//        ChannelEntity found = channelRepository.findOneById(idToSearch);
//        Assertions.assertEquals(3, found.getPosts().size());
//
//        found.getPosts().remove(0); // убрали один пост, который уже был в БД
//        ChannelEntity saved2 = channelRepository.save(found);
//        ChannelEntity found2 = channelRepository.findOneById(saved2.getId());
//
//        // then
//        Assertions.assertNotNull(found);
//        Assertions.assertEquals(saved.getId(), found.getId());
//        Assertions.assertEquals(2, found2.getPosts().size());
//    }


    @Test
    void deleteById_happyPath() {
        // given
        ChannelEntity toSave = generateChannel();
        List<PostEntity> posts = generatePosts();// получаем список рандомных постов


        toSave.setPosts(posts); //связываем entity между собой
        for (PostEntity p : posts) {
            p.setChannel(toSave); // а теперь каждый транзитивный entity знает про свой канал
        }


        ChannelEntity saved = channelRepository.save(toSave);// в момент сохранения, toSave
        // содержит 3 новых поста
        Integer idToSearch = saved.getId();// берем id сохраненного элемента,
        // чтобы потом его найти в БД

        // when
        channelRepository.deleteById(idToSearch);// удаляем канал
        ChannelEntity found = channelRepository.findOneById(idToSearch);

        // then
        Assertions.assertNull(found);


    }


}
