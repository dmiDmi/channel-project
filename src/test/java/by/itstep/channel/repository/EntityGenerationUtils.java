package by.itstep.channel.repository;

import by.itstep.channel.entity.ChannelEntity;
import by.itstep.channel.entity.PostEntity;
import by.itstep.channel.entity.enums.ChannelEntityType;

import java.util.Arrays;
import java.util.List;

public class EntityGenerationUtils {

    public static ChannelEntity generateChannel() {

        ChannelEntity entity = new ChannelEntity();
        entity.setName("Name" + Math.random() * 100);
        entity.setType(ChannelEntityType.PROGRAMING);

        return entity;
    }

    public static List<PostEntity> generatePosts() {

        PostEntity post1 = new PostEntity();
        post1.setContent("post1");
        post1.setTitle("title1");

        PostEntity post2 = new PostEntity();
        post2.setContent("post2");
        post2.setTitle("title2");

        PostEntity post3 = new PostEntity();
        post3.setContent("post3");
        post3.setTitle("title3");

        return Arrays.asList(post1, post2, post3);
    }
}
