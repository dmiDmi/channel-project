package by.itstep.channel.repository;

import by.itstep.channel.entity.UserEntity;
import by.itstep.channel.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ChannelApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@Test
	void contextLoads() {

//		UserEntity user = new UserEntity();
//		user.setLogin("Alex");
//		user.setPassword("2222");
//
//		UserEntity user2 = new UserEntity();
//		user2.setLogin("Bob");
//		user2.setPassword("3333");
//
//		UserEntity user3 = new UserEntity();
//		user3.setLogin("Frank");
//		user3.setPassword("4444");
//
//		userRepository.save(user);
//		userRepository.save(user2);
//		userRepository.save(user3);
//
//		List<UserEntity> foundUsers = userRepository.findAll();
//
//		System.out.println("Найдены:" + foundUsers);

	}

	@Test
	void testCreate_happyPath() {
		// given
		UserEntity toSave = generateUser();

		// when
		UserEntity saved = userRepository.save(toSave);

		// then
		Assertions.assertNotNull(saved);
		Assertions.assertNotNull(saved.getId());


	}

	@Test
	void testFindByIdLogin_happyPath() {
		// given
		UserEntity toSave = generateUser();
		UserEntity saved = userRepository.save(toSave);
		String loginToSearch = saved.getLogin();

		// when
		UserEntity found = userRepository.findByLogin(loginToSearch);

		// then
		Assertions.assertNotNull(found);
		Assertions.assertEquals(saved.getId(), found.getId());// должны совпасть
		Assertions.assertEquals(saved.getLogin(), found.getLogin());

	}

	@Test
	void testFindById_happyPath() {
		// given
		UserEntity toSave = generateUser();
		UserEntity saved = userRepository.save(toSave);
		Integer idToSearch = saved.getId();

		// when
		UserEntity found = userRepository.findOneById(idToSearch);

		// then
		Assertions.assertNotNull(found);
		Assertions.assertEquals(saved.getId(), found.getId());// должны совпасть
		Assertions.assertEquals(saved.getLogin(), found.getLogin());

	}

	// FAKER
	private UserEntity generateUser() {

		UserEntity user = new UserEntity();
		user.setLogin("Login" + Math.random() * 100);
		user.setPassword("Password" + Math.random() * 100);
		user.setImageUrl("Image" + Math.random() * 100);

		return user;
	}

}
