package by.itstep.channel.servise;

import by.itstep.channel.dto.user.UserCreateDto;
import by.itstep.channel.dto.user.UserFullDto;
import by.itstep.channel.dto.user.UserUpdateDto;
import by.itstep.channel.entity.UserEntity;
import by.itstep.channel.exception.EntityIsNotFoundException;
import by.itstep.channel.repository.UserRepository;
import by.itstep.channel.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @BeforeEach
    private void  setUp() {
        userRepository.deleteAll();
    }

    @Test
    void testFindById_happyPath() {
        //given
        UserEntity userEntity = new UserEntity();
        userEntity.setLogin("Bob");
        userEntity.setPassword("2233");
        userEntity.setEmail("@mail");
        userEntity.setImageUrl("jpg");

        UserEntity saved = userRepository.save(userEntity);

        //when
        UserFullDto foundDto = userService.findById(saved.getId());

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getEmail(), foundDto.getEmail());

    }

    @Test
    void testUpdate_happyPath() {
        //given
        UserCreateDto createDto = new UserCreateDto();
        createDto.setEmail("bob@gmail.com");
        createDto.setLogin("bob");
        createDto.setPassword("1234444");
        createDto.setImageUrl("img.jpg");

        UserFullDto createdUser = userService.create(createDto);

        UserUpdateDto updateDto = new UserUpdateDto();
        updateDto.setEmail("update@gmail.com");
        updateDto.setImageUrl("updated.jpg");
        updateDto.setId(createdUser.getId());

        //when
        UserFullDto updatedUser = userService.update(updateDto);

        //then
        Assertions.assertNotNull(updatedUser);
        Assertions.assertEquals(updatedUser.getId(), createdUser.getId());

        Assertions.assertNotEquals(updatedUser.getEmail(), createdUser.getEmail());
        Assertions.assertNotEquals(updatedUser.getProfileImageUrl(), createdUser.getProfileImageUrl());

        Assertions.assertEquals(updatedUser.getLogin(), createdUser.getLogin());
        Assertions.assertEquals(updatedUser.getEmail(), updateDto.getEmail());
        Assertions.assertEquals(updatedUser.getProfileImageUrl(), updateDto.getImageUrl());
    }

    @Test
    void testUpdate_whenNotFound() {
        //given
        UserUpdateDto updateDto = new UserUpdateDto();
        updateDto.setEmail("random@gmail.com");
        updateDto.setImageUrl("random.jpg");

        int notExistingId = 13;
        updateDto.setId(notExistingId);

        //when
        Exception exception = Assertions.assertThrows(EntityIsNotFoundException.class,
                () -> userService.update(updateDto));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));

    }


}
