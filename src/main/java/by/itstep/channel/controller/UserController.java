package by.itstep.channel.controller;

import by.itstep.channel.dto.user.UserCreateDto;
import by.itstep.channel.dto.user.UserFullDto;
import by.itstep.channel.dto.user.UserUpdateDto;
import by.itstep.channel.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(description = "Controller dedicated to message users")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users/{id}")
    @ApiOperation(value = "Find one user by id", notes = "Existing id must be specified")
    public UserFullDto getById(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping("/users")
    public Page<UserFullDto> findAll(@RequestParam int page, @RequestParam int size) {
        return userService.findAll(page, size);
    }

    @GetMapping("/users/filter")
    @ApiOperation(value = "Find all users by parameters", notes = "Actually gonna return all existing no matter what")
    public Page<UserFullDto> findAllByName(@ApiParam(required = false, defaultValue = "Bob") @RequestParam String name,
                                           @ApiParam(required = false) @RequestParam String email) {
    //    return userService.findAllByName(name);
        return userService.findAll(0, 10000);
    }

    @PostMapping("/users")
    public UserFullDto create(@Valid @RequestBody UserCreateDto createDto) {
        return userService.create(createDto);
    }

    @PutMapping("/users")
    public UserFullDto update(@RequestBody UserUpdateDto updateDto) {
        return userService.update(updateDto);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Integer id) {
        userService.deleteById(id);
    }

    @GetMapping("/test-create")
    public void test() {
        for (int i = 0; i < 10; i++) {

            UserCreateDto createDto = new UserCreateDto();
            createDto.setEmail("@mail");
            createDto.setLogin("Zesae");
            createDto.setPassword("3455");
            userService.create(createDto);
        }
    }
}
