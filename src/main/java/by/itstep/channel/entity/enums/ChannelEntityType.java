package by.itstep.channel.entity.enums;

public enum ChannelEntityType {

    PROGRAMING,
    EDUCATION,
    TOURISM

}
