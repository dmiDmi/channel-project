package by.itstep.channel.service.impl;

import by.itstep.channel.dto.user.UserCreateDto;
import by.itstep.channel.dto.user.UserFullDto;
import by.itstep.channel.dto.user.UserUpdateDto;
import by.itstep.channel.entity.UserEntity;
import by.itstep.channel.exception.EntityIsNotFoundException;
import by.itstep.channel.exception.UserCredentialsAreTakenException;
import by.itstep.channel.mapper.UserMapper;
import by.itstep.channel.repository.UserRepository;
import by.itstep.channel.service.UserService;
import com.mysql.cj.log.Log;
import liquibase.pro.packaged.A;
import liquibase.pro.packaged.E;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static by.itstep.channel.mapper.UserMapper.USER_MAPPER;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    @Transactional(readOnly = true)
    public UserFullDto findById(Integer id) {
        UserEntity foundEntity = userRepository.findById(id)
                .orElseThrow(() -> new EntityIsNotFoundException("User"));

        log.info("UserServiceImpl -> found user: {} by id: {} ", foundEntity, id);
        return USER_MAPPER.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserFullDto> findAll(int page, int size) {
        Page<UserEntity> foundEntities = userRepository.findAll(PageRequest.of(page, size));

        log.info("UserServiceImpl -> found {} users", foundEntities.getNumberOfElements());
        return foundEntities.map(USER_MAPPER::mapToFullDto);
    }

    @Override
    @Transactional()
    public UserFullDto create(UserCreateDto dto) {
        UserEntity entityToSave = USER_MAPPER.mapToEntity(dto);

        checkIfLoginOrEmailIsTaken(entityToSave);

        UserEntity savedEntity = userRepository.save(entityToSave);
        log.info("UserServiceImpl -> user {} successfully saved", savedEntity);
        return USER_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto dto) {
        UserEntity entityToUpdate = userRepository.findOneById(dto.getId());
        if (entityToUpdate == null) {
            throw new EntityIsNotFoundException("User not found" + dto.getId());
        }

        entityToUpdate.setEmail(dto.getEmail());
        entityToUpdate.setImageUrl(dto.getImageUrl());

        checkIfLoginOrEmailIsTaken(entityToUpdate);

        UserEntity updatedEntity = userRepository.save(entityToUpdate);
        log.info("UserServiceImpl -> user {} successfully updated", updatedEntity);
        return USER_MAPPER.mapToFullDto(updatedEntity);

    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if (!userRepository.existsById(id)) {
            throw new EntityIsNotFoundException("User by" + id);
        }
        userRepository.deleteById(id);
    }

    private void checkIfLoginOrEmailIsTaken(UserEntity entity) {
//        List<UserEntity> foundUsers = userRepository.findByLoginOrEmail(entity.getLogin(), entity.getEmail());
//        for (UserEntity user : foundUsers) {
//            if (!user.getId().equals(entity.getId())) {
//            throw new UserCredentialsAreTakenException(
//                    "Email: " + entity.getEmail() + " | login:" + entity.getLogin());
//            }
//        }

        boolean taken = userRepository.findByLoginOrEmail(entity.getLogin(), entity.getEmail())
                .stream()
                .anyMatch(user -> !user.getId().equals(entity.getId()));
        if (taken) {
            throw new UserCredentialsAreTakenException(
                    "Email: " + entity.getEmail() + " | login:" + entity.getLogin()
            );
        }


    }
}
