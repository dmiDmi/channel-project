package by.itstep.channel.service;

import by.itstep.channel.dto.user.post.PostCreateDto;
import by.itstep.channel.dto.user.post.PostFullDto;

public interface PostService {

    PostFullDto create(PostCreateDto createDto);
}
