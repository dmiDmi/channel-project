package by.itstep.channel.service;

import by.itstep.channel.dto.user.UserCreateDto;
import by.itstep.channel.dto.user.UserFullDto;
import by.itstep.channel.dto.user.UserUpdateDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {

    UserFullDto findById(Integer id);

    Page<UserFullDto> findAll(int page, int size);

    UserFullDto create(UserCreateDto dto);

    UserFullDto update(UserUpdateDto dto);

    void deleteById(Integer id);


}
