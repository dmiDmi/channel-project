package by.itstep.channel.exception;

public class EntityIsNotFoundException extends RuntimeException {

    public EntityIsNotFoundException(String message) {
        super(message);
    }
}
