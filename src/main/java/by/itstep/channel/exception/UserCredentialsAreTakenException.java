package by.itstep.channel.exception;

public class UserCredentialsAreTakenException extends RuntimeException{

    public UserCredentialsAreTakenException(String message) {
        super(message);
    }
}
