package by.itstep.channel.mapper;

import by.itstep.channel.dto.user.post.PostCreateDto;
import by.itstep.channel.dto.user.post.PostFullDto;
import by.itstep.channel.entity.PostEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PostMapper {

    PostMapper POST_MAPPER = Mappers.getMapper(PostMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "channel", ignore = true)
    PostEntity toEntity(PostCreateDto createDto);

    @Mapping(target = "channelId", source = "channel.id")
    PostFullDto toDto(PostEntity entity);
}
