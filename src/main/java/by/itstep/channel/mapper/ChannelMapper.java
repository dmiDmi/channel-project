package by.itstep.channel.mapper;

import by.itstep.channel.dto.user.channel.ChannelCreateDto;
import by.itstep.channel.dto.user.channel.ChannelFullDto;
import by.itstep.channel.dto.user.channel.ChannelPreviewDto;
import by.itstep.channel.entity.ChannelEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = PostMapper.class)
public interface ChannelMapper {

    ChannelMapper CHANNEL_MAPPER = Mappers.getMapper(ChannelMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "posts", ignore = true)
    ChannelEntity toEntity(ChannelCreateDto channelCreateDto);


    ChannelFullDto toFullDto(ChannelEntity channelEntity);


    ChannelPreviewDto toPreviewDto(ChannelEntity channelEntity);
}
