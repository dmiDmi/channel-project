package by.itstep.channel.mapper;

import by.itstep.channel.dto.user.UserCreateDto;
import by.itstep.channel.dto.user.UserFullDto;
import by.itstep.channel.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "profileImageUrl", source = "imageUrl")
 //   @Mapping(target = "phoneNumber", expression = "java(String.valueOf(entity.getId() * 999))")
    @Mapping(target = "phoneNumber", source = "id")
    UserFullDto mapToFullDto(UserEntity entity);

    @Mapping(target = "id", ignore = true)
    UserEntity mapToEntity(UserCreateDto dto);

    default String map(Integer number) {

        return String.valueOf(number * 999);
    }
}
