package by.itstep.channel.dto.user.post;

import lombok.Data;

@Data
public class PostUpdateDto {

    private Integer id;

    private String title;

    private String content;
}
