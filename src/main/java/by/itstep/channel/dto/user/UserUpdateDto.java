package by.itstep.channel.dto.user;

import lombok.Data;

@Data
public class UserUpdateDto {

    private Integer id;

    private String email;

    private String imageUrl;
}
