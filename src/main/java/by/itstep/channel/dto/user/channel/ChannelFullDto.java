package by.itstep.channel.dto.user.channel;

import by.itstep.channel.dto.user.post.PostFullDto;
import by.itstep.channel.entity.enums.ChannelEntityType;
import lombok.Data;

import java.util.List;

@Data
public class ChannelFullDto {

    private Integer id;

    private String name;

    private ChannelEntityType type;

    private List<PostFullDto> posts;
}
