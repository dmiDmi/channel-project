package by.itstep.channel.dto.user.channel;

import by.itstep.channel.entity.enums.ChannelEntityType;
import lombok.Data;

@Data
public class ChannelUpdateDto {

    private Integer id;

    private String name;

    private ChannelEntityType type;
}
