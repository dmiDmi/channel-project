package by.itstep.channel.dto.user.post;

import lombok.Data;

@Data
public class PostFullDto {

    private Integer id;

    private String title;

    private String content;

    private Integer channelId;
}
