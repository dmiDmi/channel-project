package by.itstep.channel.dto.user.post;

import lombok.Data;

@Data
public class PostCreateDto {

    private String title;

    private String content;

    private Integer channelId; //будет использ для поиска каннала
}
