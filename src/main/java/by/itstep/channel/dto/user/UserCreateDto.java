package by.itstep.channel.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class UserCreateDto {

    @ApiModelProperty(name = "Login", example = "bob_bobson", notes = "must be unique")
    @NotEmpty(message = "Login can not be empty!")
    private String login;

    @ApiModelProperty(example = "12345")
    @NotEmpty(message = "Password can not be empty!")
    private String password;

    @ApiModelProperty(example = "http://image.url", notes = "Link to the image file")
    private String imageUrl;

    @Email
    @ApiModelProperty(example = "bob@gmail.com", notes = "Email must be unique!")
    @NotEmpty(message = "Email can not be empty!")
    private String email;


}
