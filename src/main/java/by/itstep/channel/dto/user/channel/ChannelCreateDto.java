package by.itstep.channel.dto.user.channel;

import by.itstep.channel.entity.enums.ChannelEntityType;
import lombok.Data;

@Data
public class ChannelCreateDto {

    private String name;

    private ChannelEntityType type;
}
