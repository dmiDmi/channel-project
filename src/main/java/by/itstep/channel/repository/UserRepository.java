package by.itstep.channel.repository;

import by.itstep.channel.entity.UserEntity;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    UserEntity findByLogin(String login);

    UserEntity findOneById(Integer id);

    List<UserEntity> findByLoginOrEmail(String login, String email);

    @Query(value = "SELECT * FROM user " +
            "LIMIT :page, :size",
            nativeQuery = true)
    List<UserEntity> findAll(@Param("page") int page, @Param("size") int size);
}
