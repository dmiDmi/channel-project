package by.itstep.channel.repository;

import by.itstep.channel.entity.PostEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<PostEntity, Integer> {

    PostEntity findOneById(Integer id);
}
