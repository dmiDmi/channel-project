package by.itstep.channel.repository;

import by.itstep.channel.entity.ChannelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChannelRepository extends JpaRepository<ChannelEntity, Integer> {

    ChannelEntity findOneById(Integer id);
}
